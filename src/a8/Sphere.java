package a8;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Sphere implements Field {
    private Point mittelpunkt;
    private double radius;

    public Sphere(Point mittelpunkt, double radius) {
        this.mittelpunkt = mittelpunkt;
        assert radius > 0;
        this.radius = radius;
    }

    public Point getCenter() {
        return mittelpunkt;
    }

    public double getRadius() {
        return radius;
    }

    public double getSurface() {
        return 4 * Math.PI * Math.pow(radius, 2.0);
    }

    public double getVolume() {
        return 4.0 / 3.0 * Math.PI * Math.pow(radius, 3.0);
    }

    @Override
    public String toString() {
        return String.format("[<%s>: m%s, r:%.2f]", Sphere.class.getSimpleName(), mittelpunkt, radius);
    }
}
