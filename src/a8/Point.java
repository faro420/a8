package a8;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Point {
    private double x, y, z;
    final double EPSILON = 10e-5;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public static double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2.0) + Math.pow(p1.getY() - p2.getY(), 2.0) + Math.pow(p1.getZ() - p2.getZ(), 2.0));
    }

    @Override
    public boolean equals(Object other) {
        boolean testX = (Math.abs(x - ((Point) other).getX()) < EPSILON);
        boolean testY = (Math.abs(y - ((Point) other).getY()) < EPSILON);
        boolean testZ = (Math.abs(z - ((Point) other).getZ()) < EPSILON);
        return testX && testY && testZ;
    }

    @Override
    public String toString() {

        return String.format("[<%s>]:x=%.2f,y=%.2f,z=%.2f", Point.class.getSimpleName(), x, y, z);
    }
}
