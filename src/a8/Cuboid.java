package a8;

import java.util.Arrays;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Cuboid implements Field {
    // edges of the cuboid
    protected double edge1, edge2, edge3;
    // object variable for Points
    private Point[] points;

    // Contructor for 8 points
    public Cuboid(Point[] coordinates) {
        assert coordinates != null && coordinates.length == 8  &&  isCuboid(coordinates);
        points = coordinates;
    }

    // private method to determine whether a Cuboid is a true cuboid in 3D
    private boolean isCuboid(Point[] coordinates) {
        double[] distances = new double[28];
        int counter = 0;

        for (int i = 0; i < 8; i++) {
            for (int j = i + 1; j < 8; j++) {
                distances[counter] = Point.distance(coordinates[i],coordinates[j]);
                counter++;
            }
        }
        Arrays.sort(distances);

        boolean isCuboidTmp = true; int i = 0;
        do{
            isCuboidTmp = isCuboidTmp  &&  ( Math.abs(distances[i*4] - distances[i*4 + 3])  < EPSILON );
            i++;
        }while ( i<7 && isCuboidTmp );
        
        if (isCuboidTmp) {
            edge1 = distances[0];
            edge2 = distances[4];
            edge3 = Math.sqrt(distances[27] * distances[27] - edge1 * edge1 - edge2 * edge2);
        }
        return isCuboidTmp;
    }

    // calculates the Center of a given Cuboid
    public Point getCenter() {
        double x = 0;
        double y = 0;
        double z = 0;
        for (int i = 0; i < 8; i++) {
            x = x + points[i].getX();
            y = y + points[i].getY();
            z = z + points[i].getZ();
        }
        return new Point(x / 8.0, y / 8.0, z / 8.0);
    }

    // calculates the volume f a cuboid
    public double getVolume() {
        return edge1 * edge2 * edge3;
    }

    // calculates the surface area of a cuboid
    public double getSurface() {
        return 2 * (edge1 * edge2 + edge2 * edge3 + edge1 * edge3);
    }

    // a method for calculation of the distance between two points

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < 8; i++)
            s = s + points[i].toString() + "  ";
        return String.format("[<%s>:%s]", Cuboid.class.getSimpleName(), s);
    }
}
