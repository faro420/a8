package a8;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Cube extends Cuboid {
    public Cube(Point[] coordinates) {
        super(coordinates);
        assert isCube();
    }

    // determines whether a cuboid is a cube
    private boolean isCube() {
        return Math.abs(edge1 - edge2) < EPSILON && Math.abs(edge2 - edge3) < EPSILON;
    }

    @Override
    public double getSurface() {
        return 6 * edge1 * edge1;
    }

    @Override
    public double getVolume() {
        return Math.pow(edge1, 3.0);
    }

    @Override
    public String toString() {
        return String.format("[<%s>: %s]", Cube.class.getSimpleName(), super.toString());
    }

}
