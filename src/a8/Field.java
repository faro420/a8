package a8;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public interface Field {
    final double EPSILON = 10e-5;

    double getSurface();

    double getVolume();

    Point getCenter();
}
