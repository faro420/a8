package tests;

import a8.Field;
import a8.Point;
import a8.Sphere;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
// test class for class Sphere
public class TestSphere {
    private final double EPSILON = 10e-5;

    // tests method getCenter of class Sphere
    public boolean testGetCenter() {
        Field sphere1 = new Sphere(new Point(1.0, 2.0, 3.0), 5.0);
        Field sphere2 = new Sphere(new Point(-1.0, 0.0, -9), 2);
        return sphere1.getCenter().equals(new Point(1.0, 2.0, 3.0)) && sphere2.getCenter().equals(new Point(-1.0, 0.0, -9));
    }

    // tests method getSurface of class Sphere
    public boolean testGetSurface() {
        Field sphere1 = new Sphere(new Point(1.0, 2.0, 3.0), 5.0);
        // value via calculator
        double surface = 314.15926535897932384626433832795;
        return Math.abs(surface - sphere1.getSurface()) < EPSILON;
    }

    // tests method Volume of class Sphere
    public boolean testGetVolume() {
        Field sphere1 = new Sphere(new Point(1.0, 2.0, 3.0), 5.0);
        // value via calculator
        double volume = 523.59877559829887307710723054658;
        return Math.abs(volume - sphere1.getVolume()) < EPSILON;
    }

    // the test routine
    public static void main(String[] args) {
        TestSphere test = new TestSphere();
        boolean check = true;
        if (test.testGetCenter()) {
            System.out.println("getCenter():ok");
        } else {
            System.out.println("getCenter():fail");
            check = false;
        }
        if (test.testGetSurface()) {
            System.out.println("getSurface():ok");
        } else {
            System.out.println("getSurface()):fail");
            check = false;
        }
        if (test.testGetVolume()) {
            System.out.println("getVolume():ok");
        } else {
            System.out.println("getVolume():fail");
            check = false;
        }
        if (check) {
            System.out.println("all tests passed!");
        }
        Field sphere = new Sphere(new Point(1, 2, 3), 5);
        System.out.println(sphere.toString());
    }
}
