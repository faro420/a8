package tests;

import a8.Cube;
import a8.Field;
import a8.Point;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class TestCube {
    public static void main(String[] args) {
        Point[] points = { new Point(0, 0, 0), new Point(0, 1, 0), new Point(1, 0, 0), new Point(1, 1, 0), new Point(0, 0, 1), new Point(0, 1, 1),
                new Point(1, 0, 1), new Point(1, 1, 1) };
//        Point[] points = { new Point(-1.5, +4.9, 5.3), new Point(-7.3, -1.1,7.3), new Point(-2.3, -1.1, -0.7), new Point(0.3, -8.1, 8.3), new
//                Point(0.7, 5, 7.3), new Point(2.7, 4.9, 16.3),new Point(6.7, 3.9, 1.3), new Point(8.7, -2.1, 10.3) };
        Field cube = new Cube(points);
        System.out.println(cube.toString());
    }
}
