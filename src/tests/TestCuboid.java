package tests;

import a8.Cuboid;
import a8.Field;
import a8.Point;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class TestCuboid {
    public static void main(String[] args) {
//        Point[] points = { new Point(0, 0, 0), new Point(0, 0, 5), new Point(0, 5, 0), new Point(0, 5, 5), new Point(5, 0, 0), new Point(5, 0, 5),
//                new Point(5, 5, 0), new Point(5, 5, 5) };
        //
        // Point[] points = { new Point(0, 0, 0), new Point(0, 5, 0), new
        // Point(0, 3, 4), new Point(0, 8, 4), new Point(5, 0, 0), new Point(5,
        // 5, 0),
        // new Point(5, 3, 4), new Point(5, 8, 4) };

        // Point[] points = { new Point(-2, -3, -5), new Point(-2, -3, 5), new
        // Point(-2, 3, -5), new Point(-2, 3, 5), new Point(2, -3, -5), new
        // Point(2, -3, 5),
        // new Point(2, 3, -5), new Point(2, 3, 5) };

        // Point[] points = { new Point(-6.0, 1.5, 2.5), new Point(-1.0, 1.5,
        // 2.5), new Point(1.0, 0.0, 0.0), new Point(1.0, 0.0, 5.0), new
        // Point(1.0, 3.0, 0.0), new Point(1.0, 3.0, 5.0),
        // new Point(3.0, 1.5, 2.5), new Point(8.0, 1.5, 2.5) };

        // Point[] points = { new Point(-8.3, +4.9, 5.3), new Point(-6.3, -1.1,
        // 14.3), new Point(-2.3, -2.1, -0.7), new Point(0.3, -8.1, 8.3), new
        // Point(0.7, 10.9, 7.3), new Point(2.7, 4.9, 16.3),
        // new Point(6.7, 3.9, 1.3), new Point(8.7, -2.1, 10.3) };
        // Point[] points = { new Point(-3.3, +4.9, 5.3), new Point(-6.3, -1.1,
        // 7.3), new Point(-2.3, -2.1, -0.7), new Point(0.3, -8.1, 8.3), new
        // Point(0.7, 5, 7.3), new Point(2.7, 4.9, 16.3),
        // new Point(6.7, 3.9, 1.3), new Point(8.7, -2.1, 10.3) };
        Point[] points = { new Point(0, 0, 0), new Point(0, 1, 0), new Point(1, 0, 0), new Point(1, 1, 0), new Point(0, 0, 1), new Point(0, 1, 1),
                new Point(1, 0, 1), new Point(1, 1, 1) };
        

        Field cuboid = new Cuboid(points);
        System.out.println(cuboid.getVolume());
        
    }
}
