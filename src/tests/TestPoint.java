package tests;

import a8.Point;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
//test class for Point
public class TestPoint {
    // tests getters for x,y,z coordinates of class Point
    public boolean testGetters() {
        Point p = new Point(1.0, 2.0, 3.0);
        return p.getX() == 1.0 && p.getY() == 2.0 && p.getZ() == 3.0;
    }

    // tests equals(Point p) from class Point
    public boolean testEquals() {
        Point p = new Point(0.521, -2.33, 3.5);
        Point p2 = new Point(0.521, -2.33, 3.5);
        return p.equals(p2);
    }

    // entry point for the test
    public static void main(String[] args) {
        System.out.println("class Point");
        TestPoint p = new TestPoint();
        boolean check = true;
        if (p.testGetters()) {
            System.out.println("getters for x,y,z: ok");
        } else {
            check = false;
            System.out.println("getters for x,y,z: fail");
        }
        if (p.testEquals()) {
            System.out.println("equals(Object other): ok");
        } else {
            check = false;
            System.out.println("equals(Object other): fail");
        }
        if (check) {
            System.out.println("all tests passed!");
        }
    }
}
